/*
 * MatrixMultiplication.c
 *
 * Created: 11/09/2015 11:09:32 a.m.
 *  Author: Henk
 */ 

#include <avr/io.h>
#define FOSC 16000000					// Clock Speed
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1
void USART_Init( unsigned int ubrr)
{
	UBRR0H = (unsigned char)(ubrr>>8);	//Set baud rate
	UBRR0L = (unsigned char)ubrr;
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);		//Enable receiver and transmitter */
	UCSR0C = (1<<USBS0)|(3<<UCSZ00);	// Set frame format: 8data, 2stop bit */
}
void USART_Transmit( unsigned char data )
{
	while (!( UCSR0A & (1<<UDRE0)));	// Wait for empty transmit buffer
	UDR0 = data;						// Put data into buffer, sends the data
}
int main(void){
	char test = 't';
	USART_Init(MYUBRR);
	uint8_t ArrayMatrix[400] = {70,248,224,113,196,162,30,88,209,70,62,32,52,31,195,181,255,219,100,206,
		121,204,99,19,109,28,173,125,44,2,80,144,107,107,33,36,203,163,164,140,
		147,24,36,226,208,58,11,158,4,151,207,127,103,200,77,71,42,170,0,2,
		150,201,30,89,137,89,51,75,59,178,54,120,239,60,120,82,163,22,7,48,
		174,173,142,219,172,163,136,23,208,117,130,253,39,207,216,117,96,23,184,120,
		155,54,26,230,29,182,45,86,147,99,199,164,63,203,199,67,105,0,117,209,
		241,189,89,136,167,192,167,113,198,119,52,26,23,200,40,249,244,192,38,157,
		5,230,0,245,37,51,128,104,184,36,10,172,155,173,201,41,84,103,110,10,
		18,147,212,214,136,232,115,148,195,1,200,172,67,23,194,230,92,22,213,230,
		106,233,96,240,171,134,209,221,162,30,18,85,48,99,145,60,171,82,206,123,
		189,242,0,181,117,156,249,116,185,11,39,129,15,210,204,224,95,233,38,210,
		104,90,64,190,17,113,211,17,84,25,206,249,135,33,42,63,46,133,188,91,
		86,254,130,222,157,189,249,255,82,179,31,108,75,201,137,12,209,233,53,141,
		48,188,76,140,50,48,32,197,138,85,206,117,5,48,114,195,62,154,192,204,
		234,175,216,54,217,193,219,130,93,73,162,145,1,230,210,168,204,241,44,23,
		75,169,171,38,196,34,48,127,142,238,253,92,152,108,208,246,38,95,249,238,
		156,26,152,163,111,11,54,124,78,130,7,244,236,249,15,159,6,74,93,61,
		74,255,25,134,20,157,2,12,161,143,233,86,39,91,26,58,183,78,168,192,
		66,214,139,138,132,107,93,6,41,243,210,7,46,1,171,60,23,248,38,223,
		220,191,65,194,150,236,10,154,248,9,82,18,7,62,50,27,250,89,140,19
	};
	uint16_t ArrayResult[400] = {0};
	ASSR |= (1<< AS2);					// Enable counter2 as a Real Timer Counter
	while(1){
		uint8_t Row = 19;
		int8_t Collumn = 19;
		USART_Transmit(test);	
		TCCR2B = (1<< CS22)|(1<<CS20);	// Pre-scaler set to 128 results in 256 counts per second and start the timer -- depends on calculation time...
		for(uint16_t i=399;i>=0;i--){	// Each entry in the new matrix
			if (Collumn <0){
				Row--;
				Collumn = 19;
			}
			for(uint8_t j=19;j>=0;j--){
				ArrayResult[i] += ArrayMatrix[Collumn + (j*20)]*ArrayMatrix[(Row * 20) + j]; // Iteration carried out 20 times for each new matrix entry
			}
			Collumn--;
		}
		char timervalue = TCNT2;
		USART_Transmit(timervalue);		
		TCCR2B = 0x00;					// Stop the timer
	}
}