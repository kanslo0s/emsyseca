/*
 * MatrixMultiplication.c
 *
 * Created: 11/09/2015 11:09:32 a.m.
 *  Author: Henk
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>

volatile uint16_t overflowCount = 0;

void USART_Transmit( unsigned char data ){
	while (!( UCSR0A & (1<<UDRE0)));			// Wait for empty transmit buffer
	UDR0 = data;								// Put data into buffer, sends the data
}

void USART_Transmit_16(uint16_t data){	
	uint8_t tempdata;
	while (!( UCSR0A & (1<<UDRE0)));
	tempdata = (data >> 8);
	UDR0 = tempdata;
	tempdata = (0xFF & data);
	while (!( UCSR0A & (1<<UDRE0)));
	UDR0 = tempdata;
}

ISR(TIMER1_OVF_vect){
	USART_Transmit('t');										// Every 4s
}

int main(void){
	int8_t Row	= 19;
	int8_t Collumn = 19;
	int8_t ArrayMatrix[400] = {-6,0,-9,15,8,0,4,-4,4,3,9,-3,-12,13,-7,-12,-8,6,1,-12,
		-12,7,14,1,4,7,4,-6,-5,-13,-3,-13,-9,-3,-13,8,3,10,2,-12,
		10,-8,-8,-4,12,7,-15,-9,-2,1,0,-9,-10,7,4,-15,1,2,-2,-1,
		-5,15,-2,8,-2,14,15,-8,-5,9,-10,-14,-6,-13,6,3,6,-12,6,15,
		-6,15,1,-9,-5,-2,1,-6,8,9,6,2,-10,-4,6,-9,14,-2,8,-7,
		-7,-4,-6,-6,13,15,-3,-7,-11,5,-9,8,11,8,5,2,-2,-3,-4,-14,
		15,-12,2,7,-15,-6,-5,-7,-10,-6,11,-9,4,4,-15,2,-7,2,-2,-13,
		13,9,14,14,12,-2,9,12,-2,5,9,13,3,13,0,11,14,-14,15,3,
		-5,-13,-4,-11,-10,4,-1,-7,0,4,13,-1,-9,7,6,-10,-1,-4,7,-14,
		-2,-2,-5,7,-15,9,8,-4,-4,7,-6,-15,11,-4,-4,-5,10,3,8,12,
		7,-3,15,-9,4,-4,-14,-4,2,-7,1,12,15,-15,-11,8,0,0,8,-12,
		-6,-14,-11,-12,7,11,-13,8,2,-5,-8,10,-14,-7,12,-14,4,5,0,3,
		13,-11,6,-10,8,-6,-7,-6,-13,-12,8,7,15,-4,-7,-4,13,-10,0,-10,
		0,1,7,-6,-15,-1,-9,10,9,-7,6,6,3,-7,-8,5,1,-11,-7,4,
		-1,2,-6,6,15,3,6,-1,13,-14,-5,10,5,4,11,5,14,-8,-4,4,
		-5,-2,-11,-9,3,-12,-14,-3,-5,3,8,7,-2,-2,-6,6,8,-6,-13,0,
		-13,7,2,4,4,6,-6,-10,7,-1,-13,-5,6,-4,-6,-7,13,-2,12,-12,
		8,-6,-6,4,-14,-2,-6,13,-9,11,15,-9,14,9,4,7,-1,15,-4,-12,
		-3,3,13,-7,9,4,0,-10,-10,12,15,0,1,14,10,-11,-4,11,3,-3,
		6,14,-11,-11,-5,7,4,5,-1,-14,6,-2,0,-7,-7,5,-7,15,-14,5
	};
	int16_t ArrayResult[400] = {0};	
	
	/* UART serial communication configuration */
	UBRR0H = 0x00;											// Set baud rate to 9600 F_CPU/16/(9600 +1)
	UBRR0L = 0x68;
	UCSR0B = (1 << RXEN0)|(1 << TXEN0);						// Enable receiver and transmitter */
	UCSR0C = (1 << USBS0)|(3 << UCSZ00);					// Set frame format: 8data, 2stop bit */
	
	/* timer/counter2 configuration */			
	TIMSK1 |= (1 << TOIE1);									// Enable overflow Interrupt for timer 1
	TCCR1B |= (5 << CS10);									// Start timer, set pre-scaler to 1024
	sei();													// Enable global interrupts
									
	for(int16_t g=1000; g>0; g--)
	{
		
		for(int16_t i=399; i>=0; i--)
		{						// Each entry in the new matrix
			ArrayResult[i]=0;
			if (Collumn < 0)
			{
				Row--;
				Collumn = 19;
			}
			for(int8_t j=19; j>=0; j--)
			{						// Iteration carried out 20 times for each  matrix entry
					ArrayResult[i] += ArrayMatrix[Collumn + (j*20)]*ArrayMatrix[(Row * 20) + j]; 	
			}
			Collumn--;

		}
		Collumn = 19;
		Row = 19;
		
	}

	USART_Transmit_16(TCNT1);
	for(int16_t k=0;k<400;k++)
	{
		USART_Transmit_16(ArrayResult[k]);
	}
}